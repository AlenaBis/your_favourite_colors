import '../../styles/index.scss';

import '../CustomElement';

import '../TableOfColors/ColorInfoDisplay';
import '../TableOfColors/ColorInfo';
import '../TableOfColors/SaveInStoreButton';

import '../Button/Button';

import '../PanelOfAddingColors/PanelOfAddingColors';
import '../PanelOfAddingColors/ColorName';
import '../PanelOfAddingColors/ClosePanelButton';

import '../CustomSelect';
import '../settingsOfColorPicker';

