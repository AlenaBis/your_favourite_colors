/* eslint-disable no-unused-vars */
import ColorPickerUI from '@easylogic/colorpicker';

const colorpicker = ColorPickerUI.create({
  color: 'blue',
  position: 'inline',
  container: document.querySelector('#color-container'),
  type: 'sketch',
});

export default colorpicker;
