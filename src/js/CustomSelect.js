/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
import Select from 'select-custom';

class CustomSelect extends Select {
  constructor(select, options) {
    super(select, options);
    this.name = select.dataset.type;
  }

  getElements() {
    this.panelOptions = [
      ...this.select.querySelectorAll('.custom-select__option'),
    ];
    this.input = this.select.querySelector('.js-search');
  }

  init() {
    if (
      this.select.classList &&
        this.select.classList.contains('custom-select')
    ) {
      return;
    }
    super.init();

    this.getElements();
  }
}

function setSelects(selects) {
  if (!selects.length) return;

  const customSelectObjects = [];

  const options = {
    default: {},
  };

  selects.forEach((select) => {
    const name = select.dataset.type;
    const customSelect = new CustomSelect(select, options[name]);
    customSelect.init();
    customSelectObjects.push(customSelect);
  });
}

const selects = [...document.querySelectorAll('.js-select')];

setSelects(selects);
