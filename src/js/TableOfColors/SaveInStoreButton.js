/* eslint-disable guard-for-in */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */

import {CustomElement} from '../CustomElement';

class SaveInStoreButton extends CustomElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({mode: 'open'});
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadow.innerHTML = `
        <style>
        svg{
            fill: rgb(141, 141, 141);
            
            --size: 2.5rem;
    
            width: var(--size);
            height: var(--size);
    
            transition: fill 0.3s ease-in-out;
        }

        svg:hover{
            fill: var(--azur_blue);
        }
        </style>

        <svg viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M19 9.49323V19H6V6H15.1985L19 9.49323ZM15.5882 5L20 9.05405V20H5V5H15.5882Z"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M16 15H9V19H16V15ZM8 14V20H17V14H8Z"/>
        </svg> 

          `;
  }
}
customElements.define('save-in-store-button', SaveInStoreButton);

