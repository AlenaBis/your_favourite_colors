/* eslint-disable guard-for-in */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */

import {CustomElement} from '../CustomElement';

class ColorInfoDisplay extends CustomElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({mode: 'open'});
  }

  connectedCallback() {
    this.render();
    this.toRenderColorsFromLocalStore();
  }

  render() {
    this.shadow.innerHTML = `
        <style>
        .colorInfo_noData{
            display: flex;
            align-items: center;
            justify-content: center;

            min-height: 5.5rem;
            padding-top: 3rem;

            box-sizing: border-box;
            
            color: var(--smoky_white);

            font-size: 1.1rem;
            line-height: 1.5rem;
            font-weight: 500;
        }
        </style>

        <div class="colorInfo_noData"><span>You haven't added any colors...</span></div>

          `;
  }
}
customElements.define('color-info-display', ColorInfoDisplay);

