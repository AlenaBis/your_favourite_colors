/* eslint-disable max-len */
/* eslint-disable require-jsdoc */

class ColorInfo extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({mode: 'open'});
  }

  get colorName() {
    return this.getAttribute('colorname');
  }

  set colorName(content) {
    return this.setAttribute('colorname', content);
  }

  get colorType() {
    return this.getAttribute('colortype');
  }

  set colorType(content) {
    return this.setAttribute('colortype', content);
  }

  get colorCode() {
    return this.getAttribute('colorcode');
  }

  set colorCode(content) {
    return this.setAttribute('colorcode', content);
  }

  static get observedAttributes() {
    return ['colorname', 'colortype', 'colorcode'];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    // console.log(name);
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadow.innerHTML = `
      <style>
      .colorInfo__cell{
        background-color: var(--light_grey);
        height: 100%;

        color: var(--smoky_white);

        font-size: 1.1rem;
        line-height: 1.5rem;
        font-weight: 500;

        box-sizing: border-box;
        padding: 1.5rem;

        display: flex;
        align-items: center;
        justify-content: flex-start;
      }

      .colorInfo__cell > span{
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2;

        overflow: hidden; 
      }

      .colorInfo__cell_upperCase{
        text-transform: uppercase;
      }

      .colorInfo__cell_colorPreview{
        position: relative;

        --size: 4.1rem;

        min-width: var(--size);
        min-height: var(--size);
      }

      .colorInfo__cell_colorPreview:after{
        content: '';

        width: var(--size);
        height: var(--size);

        position: absolute;
        top: 50%;
        right: 50%;
        transform: translate(50%, -50%);

        background-color: ${this.colorCode};
      }

      .colorInfo__button_edit{
        stroke: #8D8D8D;
        --width: 1.3rem;

        width: var(--width);
        height: auto;

        transition: stroke 0.3s ease-in-out;
      }

      .colorInfo__cell_edit{
        cursor: pointer;

        display: flex;
        align-items: center;
        justify-content: center;
      }

      .colorInfo__cell_edit:hover .colorInfo__button_edit{
          stroke: var(--azur_blue);
      }

      .colorInfo__button_delete{
        fill: #8D8D8D;
        --width: 1.1rem;

        width: var(--width);
        height: auto;

        transition: fill 0.3s ease-in-out;
      }

      .colorInfo__button_delete rect{
        width: var(--width);
      }

      .colorInfo__cell_delete{
        cursor: pointer;

        display: flex;
        align-items: center;
        justify-content: center;
      }

      .colorInfo__cell_delete:hover .colorInfo__button_delete{
        fill: #CA4C4C;
      }
      </style>

      <div class="colorInfo__cell colorInfo__cell_colorPreview"></div>
      <div class="colorInfo__cell" data-bind="name"><span></span></div>
      <div class="colorInfo__cell" data-bind="type"></div>
      <div class="colorInfo__cell colorInfo__cell_upperCase" data-bind="color"></div>
      <div class="colorInfo__cell colorInfo__cell_edit">
      <svg class="colorInfo__button_edit" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M10.8701 1.60447C11.0429 1.41283 11.2481 1.26081 11.4739 1.1571C11.6997 1.05338 11.9417 1 12.1861 1C12.4306 1 12.6726 1.05338 12.8984 1.1571C13.1242 1.26081 13.3293 1.41283 13.5022 1.60447C13.675 1.79611 13.8121 2.02362 13.9056 2.27401C13.9991 2.5244 14.0473 2.79277 14.0473 3.06379C14.0473 3.33481 13.9991 3.60317 13.9056 3.85356C13.8121 4.10395 13.675 4.33146 13.5022 4.5231L4.61905 14.3735L1 15.468L1.98701 11.4549L10.8701 1.60447Z" stroke-linecap="round" stroke-linejoin="round"/>
      </svg>

      </div>
      <div class="colorInfo__cell colorInfo__cell_delete">
      <svg class="colorInfo__button_delete" viewBox="0 0 11 13" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M11 1H0L1.26923 13H9.73077L11 1ZM9.88865 2H1.11135L2.16904 12H8.83096L9.88865 2Z"/>
          <rect height="2"/>
      </svg>
      </div>
        `;
  }
}
customElements.define('color-info', ColorInfo);
