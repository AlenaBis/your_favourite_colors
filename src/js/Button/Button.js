/* eslint-disable max-len */
/* eslint-disable require-jsdoc */

import {CustomElement} from '../CustomElement';

const buttonToAdd = document.querySelector('#button_toAdd');
class Button extends CustomElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({mode: 'open'});
  }

  get text() {
    return this.getAttribute('text');
  }

  set text(content) {
    this.setAttribute('text', content);
  }

  get typeOfAction() {
    return this.getAttribute('typeOfAction');
  }

  set typeOfAction(type) {
    this.setAttribute('typeOfAction', type);
  }

  static get observedAttributes() {
    return ['text', 'typeofaction'];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (this.typeOfAction === 'toOpenPanelofAddingColors') {
      this.addEventListener('click', () => {
        this.toOpenPanelofAddingColors('toAdd');
        document.querySelector('panel-for-adding-colors').querySelector('button-action').setAttribute('typeofaction', 'toAddColor');
      });
    }

    if (name === 'typeofaction') {
      if (newValue === 'toEditColor') {
        this.shadowRoot.querySelector('.button_js').textContent = 'Edit color';
      } else {
        if (this.shadowRoot.querySelector('.button_js')) {
          this.shadowRoot.querySelector('.button_js').textContent = 'Add color';
        }
      }
    }
  }

  connectedCallback() {
    this.render();

    buttonToAdd?.setAttribute('typeOfAction', 'toAddColor');
  }

  render() {
    this.shadow.innerHTML = `
    <style>
      .button{
        background-color: var(--dark_grey);
        box-sizing: border-box;

        padding: 1rem 6.8rem;
        display: flex;
        align-items: center;
        justify-content: center;

        outline: none;
        border: 0.7px solid var(--azur_blue);
        border-radius: 10rem;

        cursor: pointer;

        font-family: 'Lato', sans-serif;
        color: white;

        font-size: 1.2rem;
        line-height: 1.4rem;
        font-weight: 600;

        margin: 0 auto;

        transition: filter 0.3s ease-in-out;
      }

      @media screen and (min-width:768px) {
        .button:hover{
          filter: drop-shadow(0px 0px 10px rgba(83, 203, 241, 0.2));
        }
      }
      
    </style>

          <button class="button button_js">${this.text}</button>
          `;
  }
}
customElements.define('button-action', Button);

