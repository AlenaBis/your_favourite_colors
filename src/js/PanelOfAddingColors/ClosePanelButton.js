/* eslint-disable guard-for-in */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */

import {CustomElement} from '../CustomElement';

class ClosePanelButton extends CustomElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({mode: 'open'});
  }

  connectedCallback() {
    this.render();
    const panel = document.querySelector('.panelOfAddingColors_js');

    this.addEventListener('click', () => {
      panel.setAttribute('panelvisible', false);
    });
  }

  render() {
    this.shadow.innerHTML = `
        <style>
        .closePanel_stroke{
          width: 1.5rem;

          fill: #777777;
          transition: fill 0.3s ease-in-out;

        }
        .closePanel_stroke:hover{
          fill: #CA4C4C;
        }
        </style>
        <svg class="closePanel_stroke" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="1.06064" height="11.6671" transform="matrix(0.707115 0.707099 -0.707115 0.707099 8.25 0)" />
          <rect width="1.06064" height="11.6671" transform="matrix(-0.707114 0.707099 -0.707114 -0.707099 8.99988 8.25)"/>
        </svg>
          
          `;
  }
}
customElements.define('close-panel-button', ClosePanelButton);

