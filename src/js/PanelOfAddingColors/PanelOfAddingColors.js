/* eslint-disable max-len */
/* eslint-disable require-jsdoc */

import {CustomElement} from '../CustomElement';
class PanelOfAddingColors extends CustomElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({mode: 'open'});
  }

  get panelType() {
    return this.getAttribute('panelType');
  }

  set panelType(content) {
    this.setAttribute('panelType', content);
  }

  get panelVisible() {
    return this.getAttribute('panelvisible');
  }

  set panelVisible(content) {
    this.setAttribute('panelvisible', content);
  }

  get panelName() {
    return this.getAttribute('panelName');
  }

  set panelName(content) {
    this.setAttribute('panelName', content);
  }

  static get observedAttributes() {
    return ['paneltype', 'panelname', 'panelvisible'];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'panelvisible') {
      if (newValue === 'true') {
        this.classList.add('panelOfAddingColors_visible');
      };
      if (newValue === 'false') {
        this.classList.remove('panelOfAddingColors_visible');
      }
    }

    const panelTitile = this.shadowRoot.querySelector('.panel__title_js');

    if (name === 'paneltype') {
      if (newValue === 'editColor') {
        panelTitile.innerHTML = 'Edit color';
      } else {
        if (panelTitile) {
          panelTitile.innerHTML = 'Adding color';
        }
      }
    }
  }

  connectedCallback() {
    this.render();

    document.querySelector('panel-for-adding-colors').querySelector('button-action').shadowRoot.querySelector('button').style.backgroundColor = '#53cbf1';
  }

  render() {
    this.shadow.innerHTML = `
    <style>
    .title{
      color: white;

      margin: 0 0 2.7rem;

      font-family: 'Lato Regular', sans-serif;
      font-weight: 600;
      font-size: 1.8rem;
      line-height: 2.2rem;
    }

    </style>

      <h2 class="title panel__title_js">Adding color</h2>
      <slot name="panel-close-button"></slot>
      <slot name='panel-color-name'></slot>
      <slot name='panel-color-type'></slot>

      <slot name='color-picker'></slot>
      <slot name='panel-button'></slot>

    `;
  }
}
customElements.define('panel-for-adding-colors', PanelOfAddingColors);


