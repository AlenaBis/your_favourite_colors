/* eslint-disable max-len */
/* eslint-disable require-jsdoc */

class ColorName extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({mode: 'open'});
  }

  get nameValue() {
    return this.getAttribute('nameValue');
  }

  set nameValue(content) {
    this.setAttribute('nameValue', content);
  }

  static get observedAttribute() {
    return ['nameValue'];
  }

  attributeChangedCallback(name, oldValue, newValue) {
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.shadow.innerHTML = `
      <style>
        .colorName__label{
            font-family: 'Lato Regular', sans-serif;
            font-weight: 700;
            font-size: 1.2rem;
            line-height: 2rem;

            letter-spacing: 0.01em;

            color: var(--smoky_white);
        }

        .colorName__input{
            border-radius: .6rem;

            box-sizing: border-box;
            padding: 1rem 1.5rem;

            overflow: hidden;
            text-overflow: ellipsis;

            font-family: 'Lato Regular', sans-serif;
            font-weight: 700;
            font-size: 1.2rem;
            line-height: 2rem;

            letter-spacing: 0.01em;

            color: var(--smoky_white);

            border: 1px solid #5F5F5F;
            outline: none;
            background-color: #424242;

            min-width: 17.5rem;
            max-width: 17.5rem;
        }
        @media screen and (max-width: 710px) {
          .colorName__input{
            max-width: 100%;
            flex-basis: 50%;
          }
        }
      </style>
  
            <label class="colorName__label" for="color-name">Color name</label>
            <input class="colorName__input" type="text" id="color-name" placeholder="Type name...">
  
      `;
  }
}
customElements.define('choose-color-name', ColorName);


