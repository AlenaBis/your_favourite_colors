/* eslint-disable guard-for-in */
/* eslint-disable require-jsdoc */
/* eslint-disable max-len */

import colorpicker from './settingsOfColorPicker';
const panel = document.querySelector('.panelOfAddingColors_js');

export class CustomElement extends HTMLElement {
  constructor() {
    super();

    this.state = [];
    this.counter = 0;
  }

  toOpenPanelofAddingColors(type) {
    panel.setAttribute('panelvisible', true);
    panel.setAttribute('paneltype', type);
  }

  isCustomElement(element) {
    return Object.getPrototypeOf(customElements.get(element.tagName.toLowerCase())).name === 'CustomElement';
  }

  toSaveColorinLocalStore(state) {
    const storageColorsArr = localStorage.getItem('favouriteColors');
    const favouriteColors = state;

    if (!storageColorsArr || storageColorsArr === 'undefined') {
      return localStorage.setItem('favouriteColors', JSON.stringify(favouriteColors));
    }

    let storedColors = JSON.parse(localStorage.getItem('favouriteColors'));
    storedColors = favouriteColors;
    localStorage.setItem('favouriteColors', JSON.stringify(storedColors));
  }

  toSetStoredColors(button) {
    let {value: colorName} = document.querySelector('.colorName_js').shadowRoot.querySelector('input');
    const {value: colorType} = document.querySelector('.colorType_js').querySelector('select');
    const hex = colorpicker.getColor('hex');

    if (colorName.trim() === '') {
      colorName = `${colorType} color`;
    }

    const favouriteColor = {name: colorName, type: colorType, color: hex};

    const arrOfColors = this.state;
    const isExistedColor = arrOfColors.filter((color) => color.color === hex);
    const isExistedName = arrOfColors.filter((color) => color.name === colorName);

    const attr = button.getAttribute('typeofaction');

    if (attr === 'toEditColor') {
      const index = this.counter-1;

      this.state[index].name = colorName;
      this.state[index].type = colorType;
      this.state[index].color = hex;

      const panel = document.querySelector('.panelOfAddingColors_js');
      panel.setAttribute('panelvisible', false);

      return this.getUpdatedComponent(favouriteColor, this.counter);
    }

    if (isExistedColor.length > 0) {
      alert(`You have already added a color ${hex}`);
    } else if (isExistedName.length > 0) {
      alert(`You already have a color with that name. Remove the color from the table or choose another name`);
    } else {
      this.setState(favouriteColor);
      document.querySelector('.colorName_js').shadowRoot.querySelector('input').value = '';
    }
  }

  toSetCurrentColorForPanel(counter) {
    const arr = this.state;

    this.counter = counter;

    const y = arr.find((el, i) => i+1 == counter);

    const nameCol = document.querySelector('.colorName_js').shadowRoot.querySelector('input');
    const typeCol = document.querySelector('.colorType_js').querySelector('select');
    const code = y.color;

    nameCol.value = y.name;
    typeCol.value = y.type;

    colorpicker.initColor(code);
  }

  toOpenPanelForEditingColor(counter) {
    this.toOpenPanelofAddingColors('editColor');

    document.querySelector('panel-for-adding-colors').querySelector('button-action').setAttribute('typeofaction', 'toEditColor');

    this.toSetCurrentColorForPanel(counter);
  }

  toRenderColorsFromLocalStore() {
    const favouriteColors = localStorage.getItem('favouriteColors');
    const storageColorsArr = favouriteColors && JSON.parse(favouriteColors);

    if (favouriteColors) {
      storageColorsArr?.map((color) => {
        this.setState(color);
      });
    }

    const buttonAddColor = document.querySelector('panel-for-adding-colors').querySelector('button-action[typeofaction="toAddColor"]');
    buttonAddColor?.addEventListener('click', () => this.toSetStoredColors(buttonAddColor));

    const deleteButtons = document.querySelector('.colorInfoDisplay_js').shadowRoot.querySelectorAll('.colorInfo__cell_delete_js');
    const editButtons = document.querySelector('.colorInfoDisplay_js').shadowRoot.querySelectorAll('.colorInfo__cell_edit_js');

    deleteButtons?.forEach((button) => {
      const counter = button.getAttribute('component');
      button?.addEventListener('click', () => this.toDeleteColor(counter), {once: true});
    });

    editButtons?.forEach((button) => {
      const counter = button.getAttribute('component');
      button?.addEventListener('click', () => this.toOpenPanelForEditingColor(counter));
    });
  }

  toDeleteColor(counter) {
    this.state.splice(counter-1, 1);

    const infoColor = document.querySelector('.colorInfoDisplay_js').shadowRoot.querySelector(`.component_${counter}`);
    infoColor?.remove();
  }

  updateBindings(prop, value = '', counter) {
    const bindings = [...document.querySelector('.colorInfoDisplay_js').shadowRoot.querySelectorAll(`[data-bind="${prop}_${counter}"]`)];
    const noDataMessage = document.querySelector('.colorInfoDisplay_js').shadowRoot.querySelector('.colorInfo_noData');
    noDataMessage?.remove();

    bindings.forEach((node) => {
      const dataProp = node.dataset.bind;
      const bindProp = dataProp.includes(':') ? dataProp.split(':').shift() : dataProp;
      const bindValue = dataProp.includes('.') ? dataProp.split('.').slice(1).reduce((obj, p) => obj[p], value) : value;
      const target = [...this.selectAll(node.tagName)].find((el) => el === node);
      const isStateUpdate = dataProp.includes(':') && this.isCustomElement(target);

        isStateUpdate ? target.setState({[`${bindProp}`]: bindValue}) :
          this.isArray(bindValue) ? target[bindProp] = bindValue : node.textContent = bindValue.toString();
    });

    const infoColor = document.querySelector('.colorInfoDisplay_js').shadowRoot.querySelector(`.component_${counter}`);

    const colorCode = infoColor.querySelector(`[data-bind="color_${counter}"]`);
    const colorPreview = infoColor.querySelector('.colorInfo__colorPreview');
    if (colorCode) {
      colorPreview.style.backgroundColor = `${colorCode.textContent}`;
    }

    const deleteButton = infoColor.querySelector('.colorInfo__cell_delete_js');
    const editButton = infoColor.querySelector('.colorInfo__cell_edit_js');
    deleteButton.addEventListener('click', () => this.toDeleteColor(counter), {once: true});

    editButton.addEventListener('click', () => this.toOpenPanelForEditingColor(counter));
  }

  getUpdatedComponent(newState, counter) {
    Object.entries(newState)
        .forEach(([key, value]) => {
          this.state[key] = this.isObject(this.state[key]) && this.isObject(value) ? {...this.state[key], ...value} : value;

          const bindKey = this.isObject(value) ? this.getBindKey(key, value) : key;
          const bindKeys = this.isArray(bindKey) ? bindKey : [bindKey];

          bindKeys.forEach((key) => this.updateBindings(key, value, counter));
        });
  }

  setState(newState) {
    this.state.push(newState);
    const maxLen = this.state.length;
    this.renderColors(newState, maxLen);

    this.getUpdatedComponent(newState, maxLen);
    document.querySelector('save-in-store-button').addEventListener('click', () => this.toSaveColorinLocalStore(this.state), {once: true});
  }

  getBindKey(key, obj) {
    return Object.keys(obj).map((k) => this.isObject(obj[k]) ? `${key}.${this.getBindKey(k, obj[k])}` : `${key}.${k}`);
  }

  isArray(arr) {
    return Array.isArray(arr);
  }

  isObject(obj) {
    return Object.prototype.toString.call(obj) === '[object Object]';
  }

  select(selector) {
    return this.shadowRoot ? this.shadowRoot.querySelector(selector) : this.querySelector(selector);
  }

  selectAll(selector) {
    return this.shadowRoot ? this.shadowRoot.querySelectorAll(selector) : this.querySelectorAll(selector);
  }

  renderColors(item, counter) {
    const infoColor = document.createElement('div');

    infoColor.className = `component_${counter} component`;

    infoColor.innerHTML = `
    <style>
    .component{
      height: 5.4rem;
      width: 100%;
      box-sizing: border-box;
      display: grid;
      grid-template-rows: 5.5rem;
      grid-template-columns: 13.2% 19.7% repeat(4, 16.6%);
      column-gap: 1px;
    }
    .colorInfo__cell{
      background-color: var(--light_grey);
      height: 100%;

      color: var(--smoky_white);

      font-size: 1.1rem;
      line-height: 1.5rem;
      font-weight: 500;

      box-sizing: border-box;
      padding: 1.5rem;

      display: flex;
      align-items: center;
      justify-content: flex-start;
    }

    .colorInfo__cell > span{
      display: -webkit-box;
      -webkit-box-orient: vertical;
      -webkit-line-clamp: 2;

      overflow: hidden; 
    }

    .colorInfo__cell_upperCase{
      text-transform: uppercase;

      span{
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2;
        overflow: hidden;
      }
    }

    .colorInfo__cell_colorPreview{
      position: relative;

      --size: 4.1rem;

      min-width: var(--size);
      min-height: var(--size);
    }

    .colorInfo__colorPreview{
      content: '';

      display: block;

      width: var(--size);
      height: var(--size);

      position: absolute;
      top: 50%;
      right: 50%;
      transform: translate(50%, -50%);

    }

    .colorInfo__button_edit{
      stroke: #8D8D8D;
      --width: 1.3rem;

      width: var(--width);
      height: auto;

      transition: stroke 0.3s ease-in-out;
    }

    .colorInfo__cell_edit{
      cursor: pointer;

      display: flex;
      align-items: center;
      justify-content: center;
    }

    .colorInfo__cell_edit:hover .colorInfo__button_edit{
        stroke: var(--azur_blue);
    }

    .colorInfo__button_delete{
      fill: #8D8D8D;
      --width: 1.1rem;

      width: var(--width);
      height: auto;

      transition: fill 0.3s ease-in-out;
    }

    .colorInfo__button_delete rect{
      width: var(--width);
    }

    .colorInfo__cell_delete{
      cursor: pointer;

      display: flex;
      align-items: center;
      justify-content: center;
    }

    .colorInfo__cell_delete:hover .colorInfo__button_delete{
      fill: #CA4C4C;
    }

    @media screen and (max-width: 710px) {
      .component{
        height: max-content;
        grid-template-rows: repeat(2, 5rem);
        grid-template-columns: repeat(4, 25%);
        row-gap: 1px;
      }

      .colorInfo__cell_edit, .colorInfo__cell_delete{
        grid-column: span 2;
      }
    }
    </style>

    <div class="colorInfo__cell colorInfo__cell_colorPreview"><span class="colorInfo__colorPreview"></span></div>
    <div class="colorInfo__cell"><span data-bind="name_${counter}"></span></div>
    <div class="colorInfo__cell" data-bind="type_${counter}"></div>
    <div class="colorInfo__cell colorInfo__cell_upperCase"><span data-bind="color_${counter}"></span></div>
    <div class="colorInfo__cell colorInfo__cell_edit colorInfo__cell_edit_js" component="${counter}">
        <svg class="colorInfo__button_edit" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M10.8701 1.60447C11.0429 1.41283 11.2481 1.26081 11.4739 1.1571C11.6997 1.05338 11.9417 1 12.1861 1C12.4306 1 12.6726 1.05338 12.8984 1.1571C13.1242 1.26081 13.3293 1.41283 13.5022 1.60447C13.675 1.79611 13.8121 2.02362 13.9056 2.27401C13.9991 2.5244 14.0473 2.79277 14.0473 3.06379C14.0473 3.33481 13.9991 3.60317 13.9056 3.85356C13.8121 4.10395 13.675 4.33146 13.5022 4.5231L4.61905 14.3735L1 15.468L1.98701 11.4549L10.8701 1.60447Z" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
    </div>
    <div class="colorInfo__cell colorInfo__cell_delete colorInfo__cell_delete_js" component="${counter}">
        <svg class="colorInfo__button_delete" viewBox="0 0 11 13" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M11 1H0L1.26923 13H9.73077L11 1ZM9.88865 2H1.11135L2.16904 12H8.83096L9.88865 2Z"/>
            <rect height="2"/>
        </svg>
    </div>
    `;

    document.querySelector('.colorInfoDisplay_js').shadowRoot.appendChild(infoColor);
  }
}


